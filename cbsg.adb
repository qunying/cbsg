-------------------------------------------------------------------------------
-- A simple program to output Corporate buulshit to stdout                   --
-- Copyright (C) 2021 Zhu Qun-Ying                                           --
--                                                                           --
-- SPDX-License-Identifier: MIT                                              --
--                                                                           --
-- Permission is hereby granted, free of charge, to any person obtaining a   --
-- copy of this software and associated documentation files (the "Software"),--
-- to deal in the Software without restriction, including without limitation --
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,  --
-- and/or sell copies of the Software, and to permit persons to whom the     --
-- Software is furnished to do so, subject to the following conditions:      --
--                                                                           --
-- The above copyright notice and this permission notice shall be included   --
-- in all copies or substantial portions of the Software.                    --
--                                                                           --
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS   --
-- OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                --
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    --
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      --
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,      --
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE         --
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                    --
--                                                                           --
-- Except as contained in this notice, the names of the authors or their     --
-- institutions shall not be used in advertising or otherwise to promote the --
-- sale, use or other dealings in this Software without prior written        --
-- authorization from the authors.                                           --
--                                                                           --
-------------------------------------------------------------------------------

with Corporate_Bullshit;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Characters.Latin_1;
with Ada.Command_Line; use Ada.Command_Line;

procedure Cbsg is
   package Text_Corporate_Bullshit is new Corporate_Bullshit
     (Paragraph_Mark => Ada.Characters.Latin_1.LF & "",
      Paragraph_End_Mark => Ada.Characters.Latin_1.LF & "",
      Dialog_Mark => "");
   type Special_Tag is (Sentence, Workshop, Short_Workshop, Short_Meeting,
                        Financial_Report);
   T : Special_Tag := Sentence;

   procedure Print_Usage is
   begin
      Put_Line ("Usage: cbsg [argument]");
      Put_Line ("Argument:");
      Put_Line ("  sentence          Output a sentense (default).");
      Put_Line ("  workshop          Output a workshop.");
      Put_Line ("  short-workshop    Output a short workshop.");
      Put_Line ("  short-meeting     Output a short meeting.");
      Put_Line ("  financial-report  Output a financial report.");
      Put_Line ("  help              Output this help.");
   end Print_Usage;

begin

   if Argument_Count > 1 then
      Put_Line ("Error: only one argument is allow.");
      Print_Usage;
      return;
   end if;

   if Argument_Count = 1 then
      declare
         Arg : String := Argument (1);
      begin
         if (Arg = "sentence") then
            null;
         elsif (Arg = "workshop") then
            T := Workshop;
         elsif (Arg = "short-workshop") then
            T := Short_Workshop;
         elsif (Arg = "short-meeting") then
            T := Short_Meeting;
         elsif (Arg = "financial-report") then
            T := Financial_Report;
         elsif (Arg = "help") then
            Print_Usage;
            return;
         else
            Put_Line ("Error: unknown argument """ & Arg & """");
            Print_Usage;
            return;
         end if;
      end;
   end if;

   case T is
      when Sentence =>  Put_Line (Text_Corporate_Bullshit.Sentence);
      when Workshop => Put_Line (Text_Corporate_Bullshit.Workshop);
      when Short_Workshop => Put_Line (Text_Corporate_Bullshit.Short_Workshop);
      when Short_Meeting => Put_Line (Text_Corporate_Bullshit.Short_Meeting);
      when Financial_Report => Put_Line (Text_Corporate_Bullshit.Financial_Report);
   end case;

end Cbsg;
